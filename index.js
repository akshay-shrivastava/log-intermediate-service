const path = require('path')
const redis = require('redis');
const axios = require('axios');

const minimist = require('minimist');

const dotenv = require('./dotenv');
let args = minimist(process.argv.slice(2), {
    alias: {
        h: "help"
    },
});

if (args.h) {
    console.log(`Options:\n
    \t-r: set the root dir where to lock for the .env file\n 
    `);
    return 1;
}

const root = args.r ? args.r.toString() : path.join(__dirname, "../");
dotenv({ root });

const client = redis.createClient({
    password: process.env.REDIS_PASSWORD
});

function delay(delayInms) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(2);
      }, delayInms);
    });
  }

(async function pullQueue() {
    try {
      client.LPOP('isha::logQueue', async function(err, args) {
        if(err) {
          console.log(`isha::ssoIdQueue, Error while reading content from Redis Queue ${err}`)
        }
	if(args){
        console.log(`isha::ssoIdQueue, Queue Object ${args}`)
	let tempArr = []
	tempArr.push(JSON.parse(args))
        let data= {data: tempArr.splice(0)}
        await axios({
            url: 'https://qa-client.ieco.isha.in/uploadLogs',
            method: 'post',
            data
        })
	}
        if(!args){
            await delay(3000);
            pullQueue()
            return
        }
        pullQueue()
      })
    } catch (err) {
      console.error(`Error:: ${err}`)
    }
  })()